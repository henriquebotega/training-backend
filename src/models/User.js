const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
	{
		name: { type: String, required: true },
		admin: { type: Boolean, required: true, default: false },
		email: { type: String, required: true },
		password: { type: String, required: true },
		report_id: { type: mongoose.Schema.Types.ObjectId, ref: "Report" },
		access_token: { type: String },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("User", UserSchema);
