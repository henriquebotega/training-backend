const mongoose = require("mongoose");

const LevelSchema = new mongoose.Schema(
	{
		title: { type: String, required: true },
		number: { type: Number, required: true },
		categories_id: [{ type: mongoose.Schema.Types.ObjectId, ref: "Category" }],
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Level", LevelSchema);
