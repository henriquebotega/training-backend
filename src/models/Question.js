const mongoose = require("mongoose");

const QuestionSchema = new mongoose.Schema(
	{
		title: { type: String, required: true },
		number: { type: Number, required: true },
		category_id: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
		answers_id: [{ type: mongoose.Schema.Types.ObjectId, ref: "Answer" }],
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Question", QuestionSchema);
