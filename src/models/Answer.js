const mongoose = require("mongoose");

const AnswerSchema = new mongoose.Schema(
	{
		title: { type: String, required: true },
		is_correct: { type: Boolean, required: true, default: false },
		question_id: { type: mongoose.Schema.Types.ObjectId, ref: "Question" },
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Answer", AnswerSchema);
