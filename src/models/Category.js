const mongoose = require("mongoose");

const CategorySchema = new mongoose.Schema(
	{
		title: { type: String, required: true },
		number: { type: Number, required: true },
		level_id: { type: mongoose.Schema.Types.ObjectId, ref: "Level" },
		questions_id: [{ type: mongoose.Schema.Types.ObjectId, ref: "Question" }],
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Category", CategorySchema);
