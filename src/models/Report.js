const mongoose = require("mongoose");

const ReportSchema = new mongoose.Schema(
	{
		user_id: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
		levels: [Object],
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Report", ReportSchema);
