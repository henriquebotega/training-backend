const User = require("../models/User");
const app = require("../app");

const supertest = require("supertest");
const request = supertest(app);
const checkJWT = require("../routes");

describe("Testing all routes", () => {
	let currentToken;

	test("Check if checkJWT is valid", () => {
		expect(checkJWT).toBeDefined();
	});

	test("Should call Email", async (done) => {
		const res = await request.post("/api/login").send({
			email: "admin@servidor.com",
			password: "admin",
		});

		expect(res.status).toBe(200);
		expect(res.body).toHaveProperty("auth");
		expect(res.body).toHaveProperty("token");
		expect(res.body.auth).toBe(true);
		expect(res.body.token).toHaveLength(171);
		currentToken = res.body.token;
		done();
	});

	test("Should call Email with failed", async (done) => {
		const res = await request.post("/api/login").send({
			email: "anderson@servidor.com",
			password: "123456",
		});

		expect(res.status).toBe(500);
		done();
	});

	test("Should call Logout", async (done) => {
		const res = await request.get("/api/logout");

		expect(res.status).toBe(200);
		expect(res.body).toHaveProperty("auth");
		expect(res.body).toHaveProperty("token");
		expect(res.body.auth).toBe(false);
		expect(res.body.token).toBe(null);
		done();
	});

	test("Should call Users with failed", async (done) => {
		const res = await request.get("/api/users");
		const text = JSON.parse(res.text);

		expect(res.status).toBe(401);
		expect(text).toHaveProperty("auth");
		expect(text).toHaveProperty("message");
		expect(text.auth).toBe(false);
		expect(text.message).toBe("No token provided");
		done();
	});

	test("Should call Users", async (done) => {
		const res = await request.get("/api/users").set("x-access-token", currentToken);

		expect(res.status).toBe(200);
		expect(res.body.length).toBeGreaterThanOrEqual(1);
		done();
	});
});
