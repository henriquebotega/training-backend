require("dotenv-safe").config({ path: __dirname + "/.env" });

const express = require("express");
const routes = express.Router();
const jwt = require("jsonwebtoken");

const checkJWT = (req, res, next) => {
	var token = req.headers["x-access-token"];

	if (!token) {
		return res.status(401).send({ auth: false, message: "No token provided" });
	}

	jwt.verify(token, process.env.myTOKEN, (error, decoded) => {
		if (error) {
			if (error.message.indexOf("jwt expired") > -1) {
				return res.status(401).send({ auth: false, message: "No token provided" });
			} else {
				return res.status(500).send({ auth: false, message: "Failed to authenticate token" });
			}
		}

		req.user_id = decoded._id;
		next();
	});
};

const UserController = require("./controllers/UserController");
routes.get("/users", checkJWT, UserController.getAll);
routes.get("/users/:id", checkJWT, UserController.getByID);
routes.post("/users", UserController.incluir);
routes.put("/users/:id", checkJWT, UserController.editar);
routes.delete("/users/:id", checkJWT, UserController.excluir);

const LevelController = require("./controllers/LevelController");
routes.get("/levels", checkJWT, LevelController.getAll);
routes.get("/levels/:id", checkJWT, LevelController.getByID);
routes.post("/levels", checkJWT, LevelController.incluir);
routes.put("/levels/:id", checkJWT, LevelController.editar);
routes.delete("/levels/:id", checkJWT, LevelController.excluir);

const CategoryController = require("./controllers/CategoryController");
routes.get("/categories", checkJWT, CategoryController.getAll);
routes.get("/categories/:id", checkJWT, CategoryController.getByID);
routes.post("/categories", checkJWT, CategoryController.incluir);
routes.put("/categories/:id", checkJWT, CategoryController.editar);
routes.delete("/categories/:id", checkJWT, CategoryController.excluir);

const QuestionController = require("./controllers/QuestionController");
routes.get("/questions", checkJWT, QuestionController.getAll);
routes.get("/questions/:id", checkJWT, QuestionController.getByID);
routes.post("/questions", checkJWT, QuestionController.incluir);
routes.put("/questions/:id", checkJWT, QuestionController.editar);
routes.delete("/questions/:id", checkJWT, QuestionController.excluir);

const AnswerController = require("./controllers/AnswerController");
routes.get("/answers", checkJWT, AnswerController.getAll);
routes.get("/answers/:id", checkJWT, AnswerController.getByID);
routes.post("/answers", checkJWT, AnswerController.incluir);
routes.put("/answers/:id", checkJWT, AnswerController.editar);
routes.delete("/answers/:id", checkJWT, AnswerController.excluir);

const ReportController = require("./controllers/ReportController");
routes.get("/reports", checkJWT, ReportController.getAll);
routes.get("/reports/:id", checkJWT, ReportController.getByID);
routes.post("/reports", checkJWT, ReportController.incluir);
routes.put("/reports/:id", checkJWT, ReportController.editar);
routes.delete("/reports/:id", checkJWT, ReportController.excluir);

// Others
routes.post("/google/account", UserController.googleAccount);
routes.post("/facebook/account", UserController.facebookAccount);
routes.post("/login", UserController.login);
routes.get("/logout", UserController.logout);

routes.get("/categories/level/:level_id", checkJWT, CategoryController.getByLevel);
routes.get("/questions/:id/category/:category_id", checkJWT, QuestionController.getNextQuestion);
routes.get("/answers/question/:question_id", checkJWT, AnswerController.getAnswersByQuestion);
routes.get("/reports/user_id/:user_id", checkJWT, ReportController.getByUserId);

module.exports = routes;
