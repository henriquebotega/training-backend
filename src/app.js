const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const helmet = require("helmet");
const http = require("http");

mongoose.connect("mongodb+srv://usuario:senha@banco-rgjfo.mongodb.net/test?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
});

const app = express();
app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const server = http.createServer(app);

require("./models/Answer");
require("./models/Level");
require("./models/Category");
require("./models/Question");
require("./models/Report");
require("./models/User");

app.use("/api", require("./routes"));

module.exports = server;
