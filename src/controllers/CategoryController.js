const mongoose = require("mongoose");
const Category = mongoose.model("Category");

module.exports = {
	async getAll(req, res) {
		const registros = await Category.find({}).sort("number").populate("level_id").populate("questions_id");
		return res.send(registros);
	},

	async getByID(req, res) {
		const registro = await Category.findById(req.params.id).populate("level_id").populate("questions_id");
		return res.send(registro);
	},

	async getByLevel(req, res) {
		let level_id = req.params.level_id;

		const registros = await Category.find({ level_id: { $in: level_id } })
			.sort("number")
			.populate("level_id")
			.populate("questions_id");
		return res.send(registros);
	},

	async incluir(req, res) {
		const registro = await Category.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await Category.findByIdAndUpdate(req.params.id, req.body, { new: true });
		return res.send(registro);
	},

	async excluir(req, res) {
		await Category.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
