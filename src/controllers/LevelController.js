const mongoose = require("mongoose");
const Level = mongoose.model("Level");

module.exports = {
	async getAll(req, res) {
		const registros = await Level.find({}).sort("number").populate("categories_id");
		return res.send(registros);
	},

	async getByID(req, res) {
		const registro = await Level.findById(req.params.id).populate("categories_id");
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await Level.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await Level.findByIdAndUpdate(req.params.id, req.body, { new: true });
		return res.send(registro);
	},

	async excluir(req, res) {
		await Level.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
