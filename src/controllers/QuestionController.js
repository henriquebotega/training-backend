const mongoose = require("mongoose");
const Question = mongoose.model("Question");

module.exports = {
	async getAll(req, res) {
		const registros = await Question.find({}).sort("number").populate("category_id").populate("answers_id");
		return res.send(registros);
	},

	async getByID(req, res) {
		const registro = await Question.findById(req.params.id).populate("category_id").populate("answers_id");
		return res.send(registro);
	},

	async getNextQuestion(req, res) {
		const id = req.params.id;
		const oldRegistro = await Question.findById(id);
		const nextNumber = parseInt(oldRegistro.number) + 1;

		const category_id = req.params.category_id;
		const registro = await Question.find({ category_id: { $in: category_id }, number: { $in: nextNumber } });
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await Question.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await Question.findByIdAndUpdate(req.params.id, req.body, { new: true });
		return res.send(registro);
	},

	async excluir(req, res) {
		await Question.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
