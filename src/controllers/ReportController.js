const mongoose = require("mongoose");
const Report = mongoose.model("Report");
const Level = mongoose.model("Level");
const Category = mongoose.model("Category");
const Question = mongoose.model("Question");

module.exports = {
	async getAll(req, res) {
		const registros = await Report.find({}).populate("user_id");
		return res.send(registros);
	},

	async getByUserId(req, res) {
		let user_id = req.params.user_id;

		let registro = await Report.find({ user_id: { $in: user_id } }).populate("user_id");

		if (registro.length == 0) {
			// Busca o primeiro Level
			const firstLevel = await Level.find({ number: { $in: 1 } }).limit(1);
			level_id = firstLevel[0]._id;

			// Busca a primeira categoria
			const firstCategory = await Category.find({ level_id: { $in: level_id }, number: { $in: 1 } }).limit(1);
			category_id = firstCategory[0]._id;

			// Busca a primeira questao
			const firstQuestion = await Question.find({ category_id: { $in: category_id }, number: { $in: 1 } }).limit(1);
			question_id = firstQuestion[0]._id;

			let levels = [
				{
					[firstLevel[0].number]: {
						_id: firstLevel[0]._id,
						title: firstLevel[0].title,
						number: firstLevel[0].number,
						category: [
							{
								[firstCategory[0].number]: {
									_id: firstCategory[0]._id,
									title: firstCategory[0].title,
									number: firstCategory[0].number,
									question: {
										_id: firstQuestion[0]._id,
										title: firstQuestion[0].title,
									},
								},
							},
						],
					},
				},
			];

			const body = { user_id, levels };
			await Report.create(body);

			registro = await Report.find({ user_id: { $in: user_id } }).populate("user_id");
		}

		return res.send(registro);
	},

	async getByID(req, res) {
		const registro = await Report.findById(req.params.id).populate("user_id");
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await Report.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await Report.findByIdAndUpdate(req.params.id, req.body, { new: true });
		return res.send(registro);
	},

	async excluir(req, res) {
		await Report.findByIdAndRemove(req.params.id);
		return res.send();
	},
};
