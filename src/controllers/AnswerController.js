const mongoose = require("mongoose");
const Answer = mongoose.model("Answer");

function shuffle(o) {
	for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
}

module.exports = {
	async getAll(req, res) {
		const registros = await Answer.find({}).populate("question_id");
		return res.send(registros);
	},

	async getByID(req, res) {
		const registro = await Answer.findById(req.params.id).populate("question_id");
		return res.send(registro);
	},

	async incluir(req, res) {
		const registro = await Answer.create(req.body);
		return res.send(registro);
	},

	async editar(req, res) {
		const registro = await Answer.findByIdAndUpdate(req.params.id, req.body, { new: true });
		return res.send(registro);
	},

	async excluir(req, res) {
		await Answer.findByIdAndRemove(req.params.id);
		return res.send();
	},

	async getAnswersByQuestion(req, res) {
		const question_id = req.params.question_id;

		const colValid = await Answer.find({ question_id: question_id, is_correct: true });
		const colInvalid = await Answer.find({ question_id: question_id, is_correct: false }).limit(3);

		const registros = [...colValid, ...colInvalid];

		return res.json(shuffle(registros));
	},
};
